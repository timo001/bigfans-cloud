package com.bigfans.cartservice.dao.impl;

import com.bigfans.cartservice.dao.ProductDAO;
import com.bigfans.cartservice.model.Product;
import com.bigfans.framework.dao.MybatisDAOImpl;
import org.springframework.stereotype.Repository;

@Repository(ProductDAOImpl.BEAN_NAME)
public class ProductDAOImpl extends MybatisDAOImpl<Product> implements ProductDAO {

	public static final String BEAN_NAME = "productDAO";

}
