package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.FloorEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lichong
 * @create 2018-02-24 下午5:51
 **/
@Data
public class Floor extends FloorEntity {

    private int categotyPageSize;
    private int productPageSize;

    private List<Category> catgeoryList = new ArrayList<Category>();
    private List<Product> productList = new ArrayList<Product>();

}
