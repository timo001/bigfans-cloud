package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;


/**
 * 
 * @Title: 
 * @Description: 商品类别和商品属性中间表
 * @author lichong 
 * @date 2015年10月7日 下午12:16:53 
 * @version V1.0
 */
@Data
@Table(name="Category_Attribute")
public class CategoryAttributeEntity extends AbstractModel {

	private static final long serialVersionUID = 1L;
	
	@Column(name="category_id")
	protected String categoryId;
	@Column(name="attr_id")
	protected String attrId;
	@Column(name="order_num")
	protected Integer orderNum;
	
	public String getModule() {
		return "CategoryAttributeEntity";
	}
	
	@Override
	public String getTableName() {
		return "category_attribute";
	}
	
}
