package com.bigfans.catalogservice.service.spec;

import com.bigfans.catalogservice.model.ProductSpec;

import java.util.List;

public interface SpecService {

    ProductSpec getProductSpec(String pgId, String optionId, String valueId) throws Exception;

    List<ProductSpec> listProductSpecs(String prodId) throws Exception;

}
