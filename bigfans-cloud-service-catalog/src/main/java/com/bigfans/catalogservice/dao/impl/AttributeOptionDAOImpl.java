package com.bigfans.catalogservice.dao.impl;

import java.util.List;

import com.bigfans.catalogservice.dao.AttributeOptionDAO;
import com.bigfans.catalogservice.model.AttributeOption;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;


/**
 * 
 * @Title: 
 * @Description: 
 * @author lichong 
 * @date 2015年10月7日 下午12:39:47 
 * @version V1.0
 */
@Repository(AttributeOptionDAOImpl.BEAN_NAME)
public class AttributeOptionDAOImpl extends MybatisDAOImpl<AttributeOption> implements AttributeOptionDAO {

	public static final String BEAN_NAME = "attributeOptionDAO";
	
	@Override
	public List<AttributeOption> listByCategory(String categoryId) {
		ParameterMap params = new ParameterMap();
		params.put("categoryId", categoryId);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public List<AttributeOption> listByCategory(List<String> categoryIds) {
		ParameterMap params = new ParameterMap();
		params.put("categoryIds", categoryIds);
		return getSqlSession().selectList(className + ".list", params);
	}
}
