package com.bigfans.model.event.user;

import com.bigfans.framework.event.AbstractEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lichong
 * @create 2018-03-24 上午11:18
 **/
@Data
@NoArgsConstructor
public class UserRegisteredEvent extends AbstractEvent {

    private String userId;

    public UserRegisteredEvent(String userId) {
        this.userId = userId;
    }
}
