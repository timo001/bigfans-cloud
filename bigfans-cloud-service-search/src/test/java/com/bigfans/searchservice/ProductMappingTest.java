package com.bigfans.searchservice;

import com.bigfans.searchservice.service.ProductIndexService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SearchServiceApp.class)
public class ProductMappingTest {

    @Autowired
    private ProductIndexService productIndexService;

    @Test
    public void testCreate(){
        try {
            productIndexService.create();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
