package com.bigfans.searchservice.service;

import com.bigfans.framework.model.FTSPageBean;
import com.bigfans.searchservice.model.Category;

/**
 * 
 * @Description:
 * @author lichong
 *
 */
public interface CategoryService {

    Category getById(String catId);

    FTSPageBean<Category> search(String keyword , int start , int pagesize) throws Exception;
}
