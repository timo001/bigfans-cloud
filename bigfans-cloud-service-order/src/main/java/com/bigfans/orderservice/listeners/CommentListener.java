package com.bigfans.orderservice.listeners;

import com.bigfans.framework.kafka.KafkaConsumerBean;
import com.bigfans.framework.kafka.KafkaListener;
import com.bigfans.model.event.comment.CommentCreatedEvent;
import com.bigfans.orderservice.service.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@KafkaConsumerBean
public class CommentListener {

    @Autowired
    private OrderItemService orderItemService;

    @KafkaListener
    public void on(CommentCreatedEvent event){
        try {
            orderItemService.updateStatusToCommented(event.getOrderItemId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
