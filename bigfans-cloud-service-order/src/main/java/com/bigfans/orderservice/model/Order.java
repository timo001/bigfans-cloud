package com.bigfans.orderservice.model;

import com.bigfans.framework.utils.DateUtils;
import com.bigfans.orderservice.model.entity.OrderEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @Description:订单
 * @author lichong 
 * 2014年12月14日下午4:07:01
 *
 */
@Data
public class Order extends OrderEntity {

	private static final long serialVersionUID = -1502505888265346945L;

	private Boolean hasExpired;
	private String leftTime;
	
	// 订单条目
	private List<OrderItem> items = new ArrayList<>();

	public String getOrderStatusLabel() {
		if(status == STATUS_UNPAID){
			return "未支付";
		} else if(status == STATUS_PAID){
			return "已支付";
		} else if(status == STATUS_UNCOMMENT){
			return "未评论";
		} else if(status == STATUS_COMMENTED){
			return "已评论";
		}
		return "";
	}

	public String getCreateDateStr() {
		return DateUtils.toStringWithFormat(createDate, "yyyy-MM-dd HH:mm:ss");
	}

	public Boolean getHasExpired() {
		if(this.hasExpired == null){
			this.hasExpired = DateUtils.compare(this.createDate, DateUtils.getTheDayBeforeNow(30)) > 1;
		}
		return hasExpired;
	}

	public void addItem(OrderItem orderItem){
		this.items.add(orderItem);
	}
}
