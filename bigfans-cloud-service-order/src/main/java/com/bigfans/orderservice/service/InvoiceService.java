package com.bigfans.orderservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.orderservice.model.Invoice;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年7月10日上午9:14:39
 *
 */
public interface InvoiceService extends BaseService<Invoice> {

}
